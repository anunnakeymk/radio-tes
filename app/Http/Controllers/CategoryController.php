<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function language(){
        return view('category/language');
    }
    public function local(){
        return view('category/local');
    }
    public function location(){
        return view('category/location');
    }
    public function music(){
        return view('category/music');
    }
    public function podcast(){
        return view('category/podcast');
    }
    public function sports(){
        return view('category/sports');
    }
    public function talk(){
        return view('category/talk');
    }
}
