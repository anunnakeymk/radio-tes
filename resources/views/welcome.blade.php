<!doctype html>
<html lang = "{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <link rel  = "stylesheet" href = "/css/app.css">
        <meta charset = "utf-8">
        <meta name    = "viewport" content = "width=device-width, initial-scale=1">
        <title>Radio</title>
    </head>
    <body>
        @php
        $url = 'http://opml.radiotime.com/';
        $xml = simplexml_load_file($url);
        @endphp
                 {{-- !Radio URLs and Name --}}
        <nav class = "navbar navbar-expand-lg navbar-dark bg-dark";>
        <a   class = "navbar-brand" href = "#">Radio Rajat</a>
            <div class = "navbar-nav">
                @foreach ($xml->body->children() as $root) 
                    @php
                        $url_category  = (string) $root->attributes()->URL;   //xml urls
                        $text_category = (string) $root->attributes()->text;  //xml name station   
                        $key_category  = (string) $root-> attributes()->key;  //key radio
                    @endphp
                <a class = "nav-link" href = "/category/{{$key_category}}">| {{$text_category}} |</a>
                @endforeach
            </div>
        </nav>        
        <div class="container">
                @yield('content')
                <p>Ivan</p>
        </div>
            
        



        {{-- make views from categories to /views/category --}}
        {{-- Artisan::call('make:view',['name' => "category/{$key_category}"]); --}} 

        {{-- @php 
        print_r('<pre>');
        print_r($xml);
        print_r('<pre>');
        @endphp --}}

        
        
      
 
        <script src = "https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity                          = "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin = "anonymous"></script>
        <script src = "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity = "sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin = "anonymous"></script>
        <script src = "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity    = "sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin = "anonymous"></script>
    </body>
</html>



