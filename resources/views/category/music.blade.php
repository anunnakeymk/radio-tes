@extends('welcome')


@section('content')        
        <h1>Music</h1>

        @php
        $url = 'http://opml.radiotime.com/Browse.ashx?c=music';
        $xml = simplexml_load_file($url); 
        @endphp

        {{-- !Radio URLs and Name --}}
        @foreach ($xml->body->children() as $root) 
            @php
                $url      = (string) $root->attributes()->URL;       //xml urls
                $text     = (string) $root->attributes()->text;      //xml name station   
                $guide_id = (string) $root->attributes()->guide_id;  //key radio
            @endphp
            <a href = "/category/music/{{$guide_id}}">{{$text}}</a>
            <br>
           
        @endforeach
@endsection
        
        {{-- @php Artisan::call('make:view',['name' => "category/music/{$text}"]); @endphp --}}
       

        {{-- @php 
        print_r('<pre>');
        print_r($xml);
        print_r('<pre>');
        @endphp --}}


