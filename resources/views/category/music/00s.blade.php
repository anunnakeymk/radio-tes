@extends('welcome')

@section('content')
        <h1>2000's</h1>

        @php
        $url = 'http://opml.radiotime.com/Browse.ashx?id=g2754';
        $xml = simplexml_load_file($url); 
        @endphp

        {{-- !Radio URLs and Name --}}
        @foreach ($xml->body->outline->outline as $root)
                @php
                $url = (string) $root->attributes()->URL;                   //xml urls
                $text = (string) $root->attributes()->text;                 //!xml name station   
                
                $bitrate = (string) $root->attributes()->bitrate;
                $reliability = (string) $root->attributes()->reliability;

                $subtext = (string) $root->attributes()->subtext;
                $playing = (string) $root->attributes()->playing;
                $formats = (string) $root->attributes()->formats;
                $type = (string) $root->attributes()->type;

                $playing_image = (string) $root->attributes()->playing_image;
                $image = (string) $root->attributes()->image;

                $genre_id = (string) $root->attributes()->genre_id;
                $guide_id = (string) $root->attributes()->guide_id;
                $now_playing_id = (string) $root->attributes()->now_playing_id;
                $preset_id = (string) $root->attributes()->preset_id;
                $item = (string) $root->attributes()->item;
            
            
            
                // $ch = curl_init();

                // curl_setopt($ch, CURLOPT_URL, $url);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                // $resultUrl = curl_exec($ch);
                //echo $resultUrl;
                // if (curl_errno($ch)) {
                //     echo 'Error:' . curl_error($ch);
                // }
                // curl_close ($ch);
            
            
                @endphp

                <ul class="list-unstyled">
                    <li class="media">
                    <img src="{{$image}}" class="mr-3" alt="...">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1"><a href="{{$url}}">{{$text}}</a></h5>
                        @php    
                        echo "Bitrate: ".$bitrate.'<br />';
                        echo "Reliability: ".$reliability."%".'<br />';
                        echo "Now Playing: ".$playing.'<br />';
                        echo "Format: ".$type." ".$formats.'<br />';
                        @endphp
                    </div>
                    </li>
                </ul>

            {{-- <a href="{{$url}}">{{$text}}</a> <br />
            <img src="{{$image}}" height="60" width="60">
            <br />
            @php    
            echo "Bitrate: ".$bitrate.'<br />';
            echo "Reliability: ".$reliability."%".'<br />';
            echo "Now Playing: ".$playing.'<br />';
            echo "Format: ".$type." ".$formats.'<br />';
            @endphp
            <br /> --}}
        
            {{-- <audio src="{{$resultUrl}}" controls="controls" type="audio"></audio> --}}
            {{-- <audio controls="controls" src="{{$url}}"></audio> --}}
            {{-- !Radio URLs and Name --}}        
            {{-- @php Artisan::call('make:view',['name' => "category/music/{$text}"]); @endphp --}}

            @endforeach
            <p><============================================================></p>
        
        @foreach ($xml->body->outline[1] as $root2)
                @php
                $url2 = (string) $root2->attributes()->URL;                   //xml urls
                $text2 = (string) $root2->attributes()->text;   
                
                $bitrate2 = (string) $root2->attributes()->bitrate;
                $reliability2 = (string) $root2->attributes()->reliability;

                $subtext2 = (string) $root2->attributes()->subtext;
                $playing2 = (string) $root2->attributes()->playing;
                $formats2 = (string) $root2->attributes()->formats;
                $type2 = (string) $root->attributes()->type;

                $playing_image2 = (string) $root2->attributes()->playing_image;
                $image2 = (string) $root2->attributes()->image;

                $genre_id2 = (string) $root2->attributes()->genre_id;
                $guide_id2 = (string) $root2->attributes()->guide_id;
                $now_playing_id2 = (string) $root2->attributes()->now_playing_id;
                $preset_id2 = (string) $root2->attributes()->preset_id;
                $item2 = (string) $root2->attributes()->item;
                @endphp

                <ul class="list-unstyled">
                        <li class="media">
                        <img src="{{$image2}}" class="mr-3" alt="...">
                        <div class="media-body">
                            <h5 class="mt-0 mb-1"><a href="{{$url2}}">{{$text2}}</a></h5>
                            @php    
                            echo "Bitrate: ".$bitrate2.'<br />';
                            echo "Reliability: ".$reliability2."%".'<br />';
                            echo "Now Playing: ".$subtext2.'<br />';
                            echo "Format: ".$type2." ".$formats2.'<br />';
                            @endphp
                        </div>
                        </li>
                </ul>

                {{-- <a href="{{$url2}}">{{$text2}}</a>
                <br />
                <img src="{{$image2}}" height="60" width="60">
                <br />
                
                @php    
                echo "Bitrate: ".$bitrate2.'<br />';
                echo "Reliability: ".$reliability2."%".'<br />';
                echo "Now Playing: ".$subtext2.'<br />';
                echo "Format: ".$type2." ".$formats2.'<br />';
                @endphp --}}
        @endforeach        
                

        @php
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
            

        @endphp
        {{-- @php 
        print_r('<pre>');
        print_r($xml);
        print_r('<pre>');
        @endphp --}}
@endsection