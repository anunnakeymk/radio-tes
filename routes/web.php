<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/category/language', 'CategoryController@language');
Route::get('/category/local', 'CategoryController@local');
Route::get('/category/location', 'CategoryController@location');
Route::get('/category/music', 'CategoryController@music');
Route::get('/category/podcast', 'CategoryController@podcast');
Route::get('/category/sports', 'CategoryController@sports');
Route::get('/category/talk', 'CategoryController@talk');


Route::get('/category/music/g2754', 'MusicController@y00s');